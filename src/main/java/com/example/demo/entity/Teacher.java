package com.example.demo.entity;

import com.example.demo.enums.SubjectEnum;
import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import java.util.List;
import java.util.Set;

/**
 * @author zhang
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Entity
public class Teacher extends User {


    /**
     * 学科
     */
    private SubjectEnum subjectEnums;

    /**
     * 薪水
     */
    private double salary;

    @ManyToMany
    @JoinTable(name = "student_teacher",//中间表名
            joinColumns = @JoinColumn(name = "teacher_id"),//这个类在表中的id
            inverseJoinColumns = @JoinColumn(name = "student_id"))//反转的类在联合表中的id

    private List<Student> students;
}
