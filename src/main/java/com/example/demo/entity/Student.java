package com.example.demo.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import java.util.List;

/**
 * @author zhang
 */
@EqualsAndHashCode(callSuper = true)
@Data
@Entity
public class Student extends User {

    /**
     * 年级
     */
    private int grade;

    /**
     * 班级
     */
    private int classes;

    /**
     * 分数
     */
    private int score;

    /*@ManyToMany(mappedBy = "students")
    private List<Teacher> teachers;*/
}
