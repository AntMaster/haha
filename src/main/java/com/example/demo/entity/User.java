package com.example.demo.entity;

import com.example.demo.enums.SexEnum;

import javax.persistence.Entity;

/**
 * @author zhang
 */
@Entity
public class User extends BaseEntity {

    private String username;
    private int age;
    private String password;
    private SexEnum sexEnum;
}
