package com.example.demo;

import com.example.demo.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author zhang
 */
public interface StudentRepository extends JpaRepository<Student, Integer> {
}
