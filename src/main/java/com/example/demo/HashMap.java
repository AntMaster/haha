package com.example.demo;

import lombok.Data;

import java.util.Map;

/**
 * @author zhang
 */
public class HashMap {
    public static void main(String[] args) {
        (new HashMap()).test();
    }

    /**
     * test
     */
    private void test() {


        java.util.HashMap<String, Person> map = new java.util.HashMap<>();

        map.put("张三", new Person("张三", 21));
        map.put("李四", new Person("李四", 19));
        map.put("王五", new Person("王五", 25));
        map.put("赵六", new Person("赵六", 24));
        map.put("孙七", new Person("孙七", 32));
        map.put("周八", new Person("周八", 17));
        map.put("钱九", new Person("钱九", 24));
        map.put("吴十", new Person("吴十", 23));
        System.out.println(map);
    }


    @Data
    class Person {
        String name;
        int age;

        Person(String name, int age) {
            this.name = name;
            this.age = age;
        }
    }
}
