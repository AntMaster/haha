package com.example.demo;

import com.example.demo.entity.Student;
import com.example.demo.entity.Teacher;
import com.example.demo.enums.SubjectEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author zhang
 */
@RestController
public class Controller {




    private final TeacherRepository teacherRepository;

    private final StudentRepository studentRepository;

    @GetMapping("/")
    public String home() {
        return "Spring is here!";
    }

    @Autowired
    public Controller(TeacherRepository teacherRepository, StudentRepository studentRepository) {
        this.teacherRepository = teacherRepository;
        this.studentRepository = studentRepository;
    }

    @PostMapping("/teacher")
    public Teacher saveTeacher() {
        Teacher teacher = new Teacher();
        teacher.setSalary(123.3454);
        teacher.setSubjectEnums(SubjectEnum.LANGUAGE);
        teacher.setAvailable(false);
        return teacherRepository.save(teacher);
    }

    @PostMapping("/student")
    public Student saveStudent() {
        Student student = new Student();
        student.setGrade(1);
        student.setClasses(2);
        student.setScore(123);

        return studentRepository.save(student);
    }

    @GetMapping("/teacher")
    public List<Teacher> getTeachers() {
        return teacherRepository.findAll();
    }

    @GetMapping("/student")
    public List<Student> getStudent() {
        return studentRepository.findAll();
    }

}
